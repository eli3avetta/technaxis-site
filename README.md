# mvp-landing

This project was generated with [gulp](https://github.com/gulpjs/gulp) version 4.0.2.

## Development server

Run `gulp serve` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## DEV build

Run `gulp build:dev` to build a project for dev without minify

## PROD build

Run `gulp build:prod` to build a project for prod with minify
