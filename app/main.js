import Section1Header from './sections/section-1-header/section-1-header';

import initInputsCheckbox from './inits/init-inputs-checkbox'

export default class App {
  constructor() {
    window.onload = () => {
      // секции
      new Section1Header();

      initInputsCheckbox();
    };
  }
}

new App();
