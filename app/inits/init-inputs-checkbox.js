export default function initInputsCheckbox() {
  // без явного выставления атрибута checked на мобилках не отображалось это состояние, которое было стилизованное
  const inputCheckedElems = document.querySelectorAll('input[type="checkbox"]');
  if (inputCheckedElems && inputCheckedElems.length) {
    inputCheckedElems.forEach(input => {
      input.onchange = () => {
        if (input.checked) {
          input.setAttribute('checked', input.checked);
        } else {
          input.removeAttribute('checked');
        }
      }
    })
  }
}
