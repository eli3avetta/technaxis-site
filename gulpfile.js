const gulp = require('gulp');
const fs = require('fs');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const concat = require('gulp-concat');

const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');

const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const babel = require('babelify');
const uglify = require("gulp-uglify");

const image = require('gulp-image');

const htmlmin = require('gulp-htmlmin');
const fileinclude = require('gulp-file-include');

const clean = require('gulp-clean');
const RevAll = require('gulp-rev-all');

gulp.task('clean dist', function (done) {
  if (fs.existsSync('_dist')) {
    return gulp.src('_dist', { read: false })
        .pipe(clean());
  }
  done();
});

// перенос assets в директорию сервера
gulp.task('assets:dev', function () {
  gulp.src('app/favicon.ico').pipe(gulp.dest('_dist'));
  return gulp.src('assets/**')
      .pipe(image())
      .pipe(gulp.dest('_dist/assets'));
});

// билд стилей
gulp.task('css:dev', function () {
  return gulp.src([
    'node_modules/slim-select/dist/slimselect.min.css',
    'app/styles.scss',
  ]) // какие стили надо собрать
      .pipe(sass.sync().on('error', sass.logError)) // работа с scss
      .pipe(autoprefixer({ cascade: false })) // префиксы для разных браузеров
      .pipe(concat('styles.css')) // в какой файл объединить
      .pipe(gulp.dest('_dist'));
});

// билд js
gulp.task('js:dev', function () {
  const bundler = browserify({ entries: 'app/main.js' }, { debug: true }).transform(babel);
  return bundler.bundle()
      .on("error", function (err) {
        console.error(err);
        this.emit("end");
      })
      .pipe(source('main.js'))
      .pipe(gulp.dest('_dist'));
});

// билд html
gulp.task('html:dev', function () {
  return gulp.src('app/index.html') // указываем корневой файл
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('_dist'));
});

// запуск сервера, прослушек и билдов
gulp.task('start server', function () {
  // запуска сервера
  browserSync.init({
    server: {
      baseDir: "_dist" // от куда будут браться html файлы для севрера. Например index.html Будет доступен по адресу http://localhost:3000 или http://localhost:3000/index.html
    },
    // по дефолту http://localhost:3000, но можно менять
    // host: 'localhost',
    // port: 3000,
  });

  // запускаем на перебилд
  gulp.watch('assets/**', gulp.parallel('assets:dev'));
  gulp.watch('app/**/*.scss', gulp.parallel('css:dev'));
  gulp.watch('app/**/*.js', gulp.parallel('js:dev'));
  gulp.watch('app/**/*.html', gulp.parallel('html:dev'));

  // запускаем прослушки на изменение диста, для перезагрузки страницы
  gulp.watch('_dist/**').on('change', reload);
});

// сделает первую сборку и запустит сервер
gulp.task('serve', gulp.series('clean dist', 'assets:dev', 'css:dev', 'js:dev', 'html:dev', 'start server'));

// только собрать dev
gulp.task('build:dev', gulp.series('clean dist', 'assets:dev', 'css:dev', 'js:dev', 'html:dev'));

/////////////////////////////////////////////////////////////

// перенос assets в директорию сервера
gulp.task('assets:prod', function () {
  gulp.src('app/favicon.ico').pipe(gulp.dest('_dist'));
  return gulp.src('assets/**')
      .pipe(image())
      .pipe(gulp.dest('_dist/assets'));
});

// билд стилей для prod
gulp.task('css:prod', function () {
  return gulp.src([
    'node_modules/slim-select/dist/slimselect.min.css',
    'app/styles.scss',
  ]) // какие стили надо собрать
      .pipe(sass.sync().on('error', sass.logError)) // работа с scss
      .pipe(autoprefixer({ cascade: false })) // префиксы для разных браузеров
      .pipe(cleanCSS({ compatibility: 'ie8' })) // очищение от пробелов и переносов
      .pipe(concat('styles.css')) // в какой файл объединить
      .pipe(gulp.dest('_tmp')); // отправляем во временную директорию
});

// билд js для prod
gulp.task('js:prod', function () {
  const bundler = browserify({ entries: 'app/main.js' }, { debug: true }).transform(babel);
  return bundler.bundle()
      .on("error", function (err) {
        console.error(err);
        this.emit("end");
      })
      .pipe(source('main.js'))
      .pipe(buffer())
      // .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(uglify())
      .pipe(gulp.dest('_tmp'));
});

// билд html для prod
gulp.task('html:prod', function () {
  return gulp.src('app/index.html') // указываем корневой файл
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest('_tmp')); // отправляем во временную директорию
});

// хеш файлов
gulp.task("hash:prod", function () {
  return gulp.src('_tmp/**') // забираем из временной директории
      .pipe(RevAll.revision({ dontRenameFile: ['.html'] })) // меняем файлы все, кроме html'ов
      .pipe(gulp.dest('_dist')); // перебрасываем в дист
});

// удаление временной директории
gulp.task('clean:prod', function () {
  return gulp.src('_tmp', { read: false }) // удаляем временную директорию
      .pipe(clean());
});

// только собрать prod
gulp.task('build:prod', gulp.series('clean dist', 'assets:prod', 'css:prod', 'js:prod', 'html:prod', 'hash:prod', 'clean:prod'));
